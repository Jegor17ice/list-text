package ru.ei.listtextchoose;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import java.util.Locale;

public class WebActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        String way = "file:///android_asset/";
        int position = getIntent().getExtras().getInt("position");
        Locale local = Locale.getDefault();
        if (local.toString().equals("ru_RU")) {
            way += "ru";
        } else {
            way += "default";
        }
            way += "/";

        System.out.println(local);
        switch (position) {
            case 0:
                way += "Html.html";
                break;
            case 1:
                way += "Css.html";
                break;
            case 2:
                way += "Js.html";
                break;
        }
        callHTML(way);
    }


    public void callHTML(String way) {
        setContentView(R.layout.activity_html);
        WebView webView = findViewById(R.id.webView);
        webView.loadUrl(way);
    }

}
