package ru.ei.listtextchoose;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.list_view);
        String[] siteMake = new String[]{"HTML", "CSS", "JS"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.activity_text, siteMake);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {

                Intent intent = new Intent(MainActivity.this, WebActivity.class);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }
}
